#!/usr/bin/env python

import sys
import struct
import traceback
import time


def _float32(data, offset):
  r = struct.unpack_from('>f', data, offset)
  return r[0]

def _uint64(data, offset):
  r = struct.unpack_from('>Q', data, offset)
  return r[0]

def _ntp64(data, offset):
  r = _uint64(data, offset)
  return r

def _uint32(data, offset):
  r = struct.unpack_from('>I', data, offset)
  return r[0]

def _uint16(data, offset):
  r = struct.unpack_from('>H', data, offset)
  return r[0]

def _uint8(data, offset):
  r = struct.unpack_from('B', data, offset)
  return r[0]

def _scan_point(data, of):
    if(len(data) < of +28):
      print "\n\nDATA MISSING: of: %s" % of
      print " data len %s" % len(data)
      print " last byte index: %s" % (of +27)
      return

    x = _float32(data, of)
    y = _float32(data, of + 4)
    z = _float32(data, of + 8)
    device_id = _uint8(data, of + 16)
    layer = _uint8(data, of + 17)
    echo = _uint8(data, of + 18)
    t = _float32(data, of + 20)
    msg = {}
    msg['point'] = (x, y, z)
    msg['echo_width'] = _float32(data, of + 12)
    msg['device_id'] = device_id
    msg['layer'] = layer
    msg['echo'] = echo
    msg['timedelta'] = t
    msg['layerechoflags'] = _uint8(data, of + 24)
    return msg

def _scan_points_list(data):
  scan_point_list = []
  #of = 24
  of = 0
  nb_of_scanner_info = ord(data[of + 20])
  nb_of_scanner_info = ord(data[of + 20])
  scan_points = _uint16(data, of + 18)
  assert (scan_points == ord(data[of + 18]) << 8 | ord(data[of + 19]))
  #of = 24 + 24 + 148 * nb_of_scanner_info
  of = 24 + 148 * nb_of_scanner_info
  # print "   scan points (first offset @%s)" % of
  for i in range (0, scan_points):
    #of = 24 + 24 + 148 * nb_of_scanner_info + i * 28
    of = 24 + 148 * nb_of_scanner_info + i * 28
    # print "   point %s / %s (offset @%s)" % (i, scan_points , of)
    msg = _scan_point(data, of)
    scan_point_list.append(msg)
  return scan_point_list

def _resolution_info(data, of):
    info = [0] * 8
    for i in range (0, 8):
      info[i] = {}
      info[i]['start_angle'] = _float32(data, of + i *8)
      info[i]['resolution'] = _float32(data, of + i *8 + 4)
    return info

def _scanner_info_list(data):
  scanner_info_list = []
  #of = 24
  of = 0
  nb_of_scanner_info = _uint8(data, of + 20)
  for i in range(0, nb_of_scanner_info):
    # offset
    #of = 24 + 24 + (i * 148)
    of = 24 + (i * 148)
    info = {}
    info['device_id'] = _uint8(data, of + 0)
    info['scanner_type'] = _uint8(data, of + 1)
    info['scan_number'] = _uint16(data, of + 2)
    info['start_angle'] = _float32(data, of + 8)
    info['end_angle'] = _float32(data, of + 12)
    info['start_time'] =  _ntp64(data, of + 16)
    info['end_time'] = _ntp64(data, of + 24)
    info['start_time_from_device'] = _ntp64(data, of + 32)
    info['end_time_from_device'] = _ntp64(data, of + 40)
    info['scan_hz'] = _float32(data, of + 48)
    info['beam_tilt'] = _float32(data, of + 52)
    info['scan_flags'] = _uint32(data, of + 56)  # 32 bits
    info['yaw_angle'] = _float32(data, of + 60)
    info['pitch_angle'] = _float32(data, of + 64)
    info['roll_angle'] = _float32(data, of + 68)
    info['offset_x'] = _float32(data, of + 72)
    info['offset_y'] = _float32(data, of + 76)
    info['offset_z'] = _float32(data, of + 80)
    info['resolutions'] = _resolution_info(data, of + 84)
    scanner_info_list.append(info)
  # print "     offset of last scanner info @%s" % of
  return scanner_info_list

def unpack_ecu_scan(data):
  info ={}
  # ordata = [ '02x' % ord(x) for x in data]
  # Running live, the header has been removed
  #of = 24
  of = 0
  info['ntp_time'] = _ntp64(data, of + 0)
  info['scan_end_time_offset_us'] = _ntp64(data, of + 8)
  info['flags'] = _uint32(data, of + 12) # 32 bits
  info['scan_nb'] = _uint16(data, of + 16)
  info['nb_of_scan_points'] = _uint16(data, of + 18)
  info['nb_of_scanner_infos'] = _uint8(data, of +20)
  info['scanner_info_list'] = _scanner_info_list(data)
  info['scan_points'] = _scan_points_list(data)
  return info

def unpack_vehicle_state(data):
  # Running live, the header has been removed
  #x = struct.unpack_from(">IQIIffffffHffffffffff", data, 28)
  x = struct.unpack_from(">IQIIffffffHffffffffff", data, 0)
  info = {}
  info['micro_seconds'], \
  info['timestamp'], \
  info['x'], \
  info['y'], \
  info['course_angle'], \
  info['longitudinal_velocity'], \
  info['yaw_rate'], \
  info['steering_angle'], \
  r0, \
  info['front_wheel_angle'], \
  r1, \
  info['vehicle_width'], \
  r2, \
  info['vehicle_front_to_front_axle'], \
  info['front_axle_to_rear_axle'], \
  info['rear_axle_to_vehicle_rear'], \
  info['stear_ratio_coeff0'], \
  info['stear_ratio_coeff1'], \
  info['stear_ratio_coeff2'], \
  info['stear_ratio_coeff3'], \
  info['longitudinal_acceleration'] = x
  return info

def _set_filter_command(data):
  of = 24
  print " +00 command id %s (offset @%s)" % (data[of + 0 : of + 2], of)
  nb_of_entries = ord(data[of + 2]) << 8 | ord(data[of + 3])
  print " +02 number of entries %s (%s)" \
    % (nb_of_entries, data[of + 2: of + 4])
  for i in range(0, nb_of_entries):
    of = 24 + 4 + 2 * i
    print "  data type range %s / %s (offset @%s)" % (i, nb_of_entries, of)
    print "   - +00 start of data type %s" % data[of + 0: of + 2]
    print "   - +02 end of data type %s" % data[of + 0: of + 2]


def _get_packet_data(nb):
  # some packets have multiple data layers.
  # only the last one seems to have data
  # (pkt.data can be empty)
  data = None
  pkt = cap[nb]
  if pkt.layers[-1].layer_name != 'data':
    return None
  try:
     data = pkt.layers[-1].data.binary_value
  except:
    return None
  return data

if __name__ == "__main__":
  import ecu_pcap_reader
  filename = "ibeo-ecu-datalogger-run.pcapng"
  min_p = 0
  max_p = -1

  if len(sys.argv) >= 4:
    filename = sys.argv[1]
    min_p = int(sys.argv[2])
    max_p = int(sys.argv[3])

  messages = ecu_pcap_reader.ecu_messages(filename, min_p, max_p)

  i = 0
  for message in messages:
    first, last, info, raw_data = message
    print "%s / %s type: %s" % (i, len(messages), info['type'])
    if info['type'] == 'scan':
      data = unpack_ecu_scan(raw_data)
      nb_of_scan_points = data['nb_of_scan_points']
      nb = data['scan_nb']
      print "  scan nb: %s with  %s points" %  (nb, nb_of_scan_points)
    if info['type'] == 'vehicle_state':
      data = unpack_vehicle_state(raw_data)
      angle = data['steering_angle']
      print "  steering angle: %s" %  (angle)
    i += 1
  print "bye"


