#!/usr/bin/env python

import ecu_data_unpack

import time
import numpy as np
import rospy
from sensor_msgs.msg import *
from rospy.numpy_msg import numpy_msg

# custom ECU messages
import sick_ldmrs.msg

topics = {}

def init_ros():
  rospy.init_node('ibeo_pcap')
  node_name = rospy.get_name().split('/')[-1]

  global topics
  init_ecu_ros_topics(topics, node_name)

# now in sick_ldmrs/nodes/sickldmrs.py
# called from init_ros
def init_ecu_ros_topics(topics, node_name):
  topics['cloud'] = rospy.Publisher('/%s/cloud' % node_name,
  numpy_msg(PointCloud2),
  queue_size=10)
  topics['scanner_info'] = rospy.Publisher('/%s/scanner_info' % node_name,
        sick_ldmrs.msg.ScannerInfo,
        queue_size=10)
  topics['vehicle_state'] = rospy.Publisher('/%s/vehicle_state' % node_name,
        sick_ldmrs.msg.VehicleState,
        queue_size=10)


class IbeoEcuPublisher(object):

  def __init__(self, frame_id, topics):
    self._init_ecu_publisher(frame_id)
    self.topics = topics

  def publish_ecu_point_cloud(self, scan):
    self.scan = scan
    self.make_ecu_point_cloud()
    pc_handle = self.topics['cloud']
    pc_handle.publish(self.point_cloud)
    # also publish scanner info list
    self.make_scanner_infos()
    si_handle = self.topics['scanner_info']
    for info_msg in self.scanner_infos:
      si_handle.publish(info_msg)

  def publish_vehicle_state(self, vehicle_state):
    self.vehicle_state_data = vehicle_state
    self.make_vehicle_state()
    vs_handle = self.topics['vehicle_state']
    vs_handle.publish(self.vehicle_state)

  def make_ecu_point_cloud(self):
    self.n_points = len(self.scan['scan_points'])
    data = ''
    for i in range(0, self.n_points):
      point = self.scan['scan_points'][i]
      # TODO: shouldn't need this
      if point:
        x, y, z = point['point']
        echo_w = point['echo_width']
        time_delta = point['timedelta']
        flags = point['layerechoflags']
        device_id = point['device_id']
        echo = point['echo']
        layer = point['layer']
        #TODO: check whether negative echo_w makes sense
        data += struct.pack('ffffHBBBB', \
                             x, y, z, time_delta, echo_w, flags, device_id, echo, layer)
    self.pc_data = data
    self._fill_ecu_point_cloud()

  def make_scanner_infos(self):
    # check for a different size list than expected
    if len(self.scanner_infos) != len(self.scan['scanner_info_list']):
      self.scanner_infos =[]
      for i in range(0, len(self.scan['scanner_info_list'])):
        self.scanner_infos.append(sick_ldmrs.msg.ScannerInfo())
    # fill the data
    for i in range(0, len(self.scanner_infos)):
      self.scanner_infos[i].device_id = self.scan['scanner_info_list'][i]['device_id']
      self.scanner_infos[i].scanner_type = self.scan['scanner_info_list'][i]['scanner_type']
      self.scanner_infos[i].start_angle = self.scan['scanner_info_list'][i]['start_angle']
      self.scanner_infos[i].end_angle = self.scan['scanner_info_list'][i]['end_angle']
      self.scanner_infos[i].start_time = self.scan['scanner_info_list'][i]['start_time']
      self.scanner_infos[i].end_time = self.scan['scanner_info_list'][i]['end_time']
      self.scanner_infos[i].start_time_from_device = self.scan['scanner_info_list'][i]['start_time_from_device']
      self.scanner_infos[i].end_time_from_device = self.scan['scanner_info_list'][i]['end_time_from_device']
      self.scanner_infos[i].scan_hz = self.scan['scanner_info_list'][i]['scan_hz']
      self.scanner_infos[i].beam_tilt = self.scan['scanner_info_list'][i]['beam_tilt']
      self.scanner_infos[i].scan_flags = self.scan['scanner_info_list'][i]['scan_flags']
      self.scanner_infos[i].yaw_angle = self.scan['scanner_info_list'][i]['yaw_angle']
      self.scanner_infos[i].pitch_angle = self.scan['scanner_info_list'][i]['pitch_angle']
      self.scanner_infos[i].roll_angle = self.scan['scanner_info_list'][i]['roll_angle']
      self.scanner_infos[i].offset_x = self.scan['scanner_info_list'][i]['offset_x']
      self.scanner_infos[i].offset_y = self.scan['scanner_info_list'][i]['offset_y']
      self.scanner_infos[i].offset_z = self.scan['scanner_info_list'][i]['offset_z']
      self.scanner_infos[i].offset_z = self.scan['scanner_info_list'][i]['offset_z']
      # scanner resolutions
      self.scanner_infos[i].resolution_start_angle_1 = self.scan['scanner_info_list'][i]['resolutions'][0]['start_angle']
      self.scanner_infos[i].resolution_1 = self.scan['scanner_info_list'][i]['resolutions'][0]['resolution']
      self.scanner_infos[i].resolution_start_angle_2 = self.scan['scanner_info_list'][i]['resolutions'][1]['start_angle']
      self.scanner_infos[i].resolution_2 = self.scan['scanner_info_list'][i]['resolutions'][1]['resolution']
      self.scanner_infos[i].resolution_start_angle_3 = self.scan['scanner_info_list'][i]['resolutions'][2]['start_angle']
      self.scanner_infos[i].resolution_3 = self.scan['scanner_info_list'][i]['resolutions'][2]['resolution']
      self.scanner_infos[i].resolution_start_angle_4 = self.scan['scanner_info_list'][i]['resolutions'][3]['start_angle']
      self.scanner_infos[i].resolution_4 = self.scan['scanner_info_list'][i]['resolutions'][3]['resolution']
      self.scanner_infos[i].resolution_start_angle_5 = self.scan['scanner_info_list'][i]['resolutions'][4]['start_angle']
      self.scanner_infos[i].resolution_5 = self.scan['scanner_info_list'][i]['resolutions'][4]['resolution']
      self.scanner_infos[i].resolution_start_angle_6 = self.scan['scanner_info_list'][i]['resolutions'][5]['start_angle']
      self.scanner_infos[i].resolution_6 = self.scan['scanner_info_list'][i]['resolutions'][5]['resolution']
      self.scanner_infos[i].resolution_start_angle_7 = self.scan['scanner_info_list'][i]['resolutions'][6]['start_angle']
      self.scanner_infos[i].resolution_7 = self.scan['scanner_info_list'][i]['resolutions'][6]['resolution']
      self.scanner_infos[i].resolution_start_angle_8 = self.scan['scanner_info_list'][i]['resolutions'][7]['start_angle']
      self.scanner_infos[i].resolution_8 = self.scan['scanner_info_list'][i]['resolutions'][7]['resolution']

  def make_vehicle_state(self):
    self.vehicle_state.micro_seconds = self.vehicle_state_data['micro_seconds']
    self.vehicle_state.timestamp = self.vehicle_state_data['timestamp']
    self.vehicle_state.x = self.vehicle_state_data['x']
    self.vehicle_state.y = self.vehicle_state_data['y']
    self.vehicle_state.course_angle = self.vehicle_state_data['course_angle']
    self.vehicle_state.longitudinal_velocity = self.vehicle_state_data['longitudinal_velocity']
    self.vehicle_state.yaw_rate = self.vehicle_state_data['yaw_rate']
    self.vehicle_state.steering_angle = self.vehicle_state_data['steering_angle']
    self.vehicle_state.front_wheel_angle = self.vehicle_state_data['front_wheel_angle']
    self.vehicle_state.vehicle_width = self.vehicle_state_data['vehicle_width']
    self.vehicle_state.vehicle_front_to_front_axle = self.vehicle_state_data['vehicle_front_to_front_axle']
    self.vehicle_state.front_axle_to_rear_axle = self.vehicle_state_data['front_axle_to_rear_axle']
    self.vehicle_state.rear_axle_to_vehicle_rear = self.vehicle_state_data['rear_axle_to_vehicle_rear']
    self.vehicle_state.stear_ratio_coeff0 = self.vehicle_state_data['stear_ratio_coeff0']
    self.vehicle_state.stear_ratio_coeff1 = self.vehicle_state_data['stear_ratio_coeff1']
    self.vehicle_state.stear_ratio_coeff2 = self.vehicle_state_data['stear_ratio_coeff2']
    self.vehicle_state.stear_ratio_coeff3 = self.vehicle_state_data['stear_ratio_coeff3']
    self.vehicle_state.longitudinal_acceleration = self.vehicle_state_data['longitudinal_acceleration']

  def _init_ecu_publisher(self, frame_id):
    #print "init ecu publisher"
    scan_frequency = 25.0
    self.known_delta_t = rospy.Duration(256.0/scan_frequency)
    self.point_cloud = numpy_msg(PointCloud2)()
    self._init_ecu_point_cloud(frame_id)
    self.scan = None
    self.vehicle_state = sick_ldmrs.msg.VehicleState()
    self.scanner_infos = [sick_ldmrs.msg.ScannerInfo()] * 2


  def _init_ecu_point_cloud(self, frame_id):
        pc = self.point_cloud
        # set frame id (for the cloud topic this is just the frame_id_base)
        pc.header.frame_id = frame_id
        # there are 6 fields:
        # x,y,z,timedelta,echowidth,layerechoflags
        fields = [['x', PointField.FLOAT32, 0],
                    ['y', PointField.FLOAT32, 4],
                    ['z', PointField.FLOAT32, 8],
                    ['timedelta', PointField.FLOAT32, 12],
                    ['echowidth', PointField.UINT16, 16],
                    ['layerechoflags', PointField.UINT8, 18],
                    ['deviceid', PointField.UINT8, 19],
                    ['echo', PointField.UINT8, 20],
                    ['layer', PointField.UINT8, 21]]

        # set up PointFields that describe the layout of the data
        # blob in the message
        point_fields = []
        for field in fields:
            pf = PointField()
            pf.name = field[0]
            pf.datatype = field[1]
            pf.offset = field[2]
            pf.count = 1
            point_fields.append(pf)
        pc.fields = point_fields
        # set up other attributes
        pc.height = 1       # data is 1D
        pc.is_dense = False # unordered points
        pc.point_step = 22 # 19  # bytes per point
        # endianness check and set
        if struct.pack("h", 1) == "\000\001":
            pc.is_bigendian = True
        else:
            pc.is_bigendian = False


  def _fill_ecu_point_cloud(self):
    pc = self.point_cloud
    pc.header.stamp = rospy.get_rostime() # self.smoothtime
    pc.header.seq = self.scan['scan_nb']
    num_points = len(self.scan['scan_points'])
    pc.width = num_points
    pc.row_step = len(self.pc_data)
    pc.data = self.pc_data

def main():
  init_ros()

  filename = rospy.get_param("~pcap_file", "../../pcap/ibeo-ecu-datalogger-run.pcapng")

  min_p = 0
  max_p = -1

  if len(sys.argv) >= 4:
    filename = sys.argv[1]
    min_p = int(sys.argv[2])
    max_p = int(sys.argv[3])

  import ecu_pcap_reader
  messages = ecu_pcap_reader.ecu_messages(filename, min_p, max_p)

  pub = IbeoEcuPublisher("odom", topics)

  i = 0
  print "Starting publishing of pcap file:", filename
  while not rospy.is_shutdown():
    for msg in messages:
      time.sleep(0.1)
      first, last, info, raw_data = msg
      # print "%s / %s type: %s" % (i, len(messages), info['type'])
      if info['type'] == 'scan':
        data = ecu_data_unpack.unpack_ecu_scan(raw_data)
        # print "scan: %s" % data['scan_nb']
        pub.publish_ecu_point_cloud(data)
      elif info['type'] == 'vehicle_state':
        data  = ecu_data_unpack.unpack_vehicle_state(raw_data)
        # print "vehicle state"
        pub.publish_vehicle_state(data)
      else:
        print "\n\nIgnoring unknown message type %s" % info['type']
      i += 1

if __name__ == "__main__":
  main()
