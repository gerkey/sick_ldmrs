#!/usr/bin/env python

import sys
import struct
import traceback
import pyshark
import time


def _get_packet_data(cap, nb):
  # some packets have multiple data layers.
  # only the last one seems to have data
  # (pkt.data can be empty)
  data = None
  pkt = cap[nb]
  if pkt.layers[-1].layer_name != 'data':
    return None
  try:
     data = pkt.layers[-1].data.binary_value
  except:
    return None
  return data

def _pkt(cap, nb):
  size = 0
  ibeo = False
  pkt_type = None
  packet = {}
  pkt = cap[nb]
  try:
    packet['src'] = pkt.ip.addr
    packet['dst'] = pkt.ip.dst
    data = None
    direction = 'ecu'
    if pkt.ip.dst == '192.168.0.150':
      direction = 'logger'
    packet['direction'] = direction
    data = _get_packet_data(cap, nb)
    if data:
      size = len(data)
      if size > 16:
        if data[0] == '\xaf' and \
           data[1] == '\xfe' and \
           data[2] == '\xc0' and \
           data[3] == '\xc2':
          ibeo = True
        if data[14] == '\x22' and data[15] == '\x05':
          pkt_type = 'scan'

        if data[14] == '\x20' and data[15] == '\x10':
          pkt_type = 'set_filter'

        if data[14] == '\x20' and data[15] == '\x20':
          pkt_type = 'reply'

        if data[14] == '\x28' and data[15] == '\x07':
          pkt_type = 'vehicle_state'
  except:
    print " crash... pkt nb:", nb ,  sys.exc_info()[0]
    traceback.print_exc(file=sys.stdout)
#    raise

  packet['size'] = size
  packet['ibeo'] = ibeo
  packet['type'] = pkt_type
  # packet['data'] = data
  return packet, pkt


def _get_ecu_data(cap, nb):

  def _split_packet_from_header(data):
    header = data[0:24]
    payload = data[24:]
    return header, payload

  first = 0
  last = 0
  data = None
  start = False
  done = False
  i = nb
  byte_count = 0
  first_info = None
  last_info_packet = i

  while not done:
    info, pk = _pkt(cap, i)
    if info['ibeo'] and info['direction'] == 'logger':
      if info['type'] == 'vehicle_state' and not start:
        new_data = _get_packet_data(cap, i)
        header, payload = _split_packet_from_header(new_data)
        return payload, i, i, info
      if start:
        done = True
        last = i -1
        break
      if info['type'] == 'scan':
        start = True
        first = i
        first_info = info
    if start and info['size'] and info['direction'] == 'logger':
      new_data = _get_packet_data(cap, i)
      if new_data:
        if not data:
          data = new_data
        else:
          data += new_data
          last_info_packet = i
    if data:
      byte_count = len(data)
    i += 1
  header, payload = _split_packet_from_header(data)
  return payload, first, last_info_packet, first_info

def ecu_messages(filename = "ibeo-ecu-datalogger-run.pcapng" , _min_nb = 0, _max_nb = -1):
  print "loading packet file:", filename
  cap = pyshark.FileCapture(filename)
  cap.load_packets()
  print "%s packets " % len(cap)

  min_nb = _min_nb
  max_nb = _max_nb
  print "assembling scan packets..."
  if max_nb == -1:
     max_nb = len(cap)
  ecu_datas = []
  i = min_nb
  done = False
  count = 0
  while i < max_nb:
    data, first, last, info = _get_ecu_data(cap, i)
    i = last + 1
    size = 0
    if data:
       size = len(data)
    count += 1
    ecu_datas.append( [first, last, info, data] )
  print "found %s ecu messages between packets %s and %s" % \
    (len(ecu_datas), min_nb, max_nb)
  return ecu_datas


if __name__ == "__main__":

  filename = "ibeo-ecu-datalogger-run.pcapng"
  min_p = 0
  max_p = -1

  print sys.argv
  if len(sys.argv) >= 4:
    filename = sys.argv[1]
    min_p = int(sys.argv[2])
    max_p = int(sys.argv[3])
  messages = ecu_messages(filename, min_p, max_p)
  i = 0
  for msg in messages:
    first, last, info , data  = msg
    print "msg %s / %s: [%s %s] %s" % (i, len(messages), first, last, info)
    i += 1
  print "bye"





