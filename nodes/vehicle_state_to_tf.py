#!/usr/bin/env python
import rospy
from sick_ldmrs.msg import VehicleState


import tf

def handle_vehicle_state(msg):
    br = tf.TransformBroadcaster()
    br.sendTransform((msg.x, msg.y, 0),
                     tf.transformations.quaternion_from_euler(0, 0, msg.course_angle),
                     rospy.Time.now(),
                     "base_link",
                     "odom")

if __name__ == '__main__':
    rospy.init_node('vehicle_state_tf_broadcaster')
    rospy.Subscriber('vehicle_state',
                     VehicleState,
                     handle_vehicle_state,
                     queue_size=10)
    rospy.spin()

