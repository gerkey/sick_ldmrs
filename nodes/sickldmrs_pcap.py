#!/usr/bin/env python

import sys

from sick_ldmrs import ecu_ros_pub

if __name__ == '__main__':
	sys.exit(ecu_ros_pub.main())
